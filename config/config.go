package config

import (
	"os"
)

/**
	ENV ----------------------------------------------------------------------------------------------------------------
 */
func GetEnv() string {
	env := os.Getenv("ENV")
	if env == "" {
		env = "DEMO"
	}
	return env
}

/**
	DB -----------------------------------------------------------------------------------------------------------------
 */

//GetDBHost - Get DB Host from ENV
func GetDBHost() string {
	dbHost := os.Getenv("DB_HOST")
	if dbHost == "" {
		dbHost = "localhost"
	}
	return dbHost
}

//GetDBPort - Get DB Port from ENV
func GetDBPort() string {
	dbPort := os.Getenv("DB_PORT")
	if dbPort == "" {
		dbPort = "5432"
	}
	return dbPort
}

//GetDBName - Get DB Name from ENV
func GetDBName() string {
	dbName := os.Getenv("DB_NAME")
	if dbName == "" {
		dbName = "dms"
	}
	return dbName
}

//GetDBUser - Get DB User from ENV
func GetDBUser() string {
	dbUser := os.Getenv("DB_USER")
	if dbUser == "" {
		dbUser = "dms"
	}
	return dbUser
}

//GetDBPassword - Get DB Password from ENV
func GetDBPassword() string {
	dbPassword := os.Getenv("DB_PASSWORD")
	if dbPassword == "" {
		dbPassword = "dms"
	}
	return dbPassword
}