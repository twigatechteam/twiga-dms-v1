# Debian Image with latest Version of Go Installed
FROM golang:alpine

# Copy Local Project to Container's Workspace
ADD . /go/src/twiga-dms-v1


# Install git in our alpine image
RUN apk add --no-cache git mercurial \
    && go get ./... \
    && apk del git mercurial

ADD cert.pem  /go/bin/

ADD key.pem  /go/bin/


# Service listens on port 443.
EXPOSE 443

WORKDIR /go/bin

# Run when the container starts
CMD "./twiga-dms-v1"

