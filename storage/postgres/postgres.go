package postgres

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"twiga-mpesa-service/config"
)

var (
	dbHost     = config.GetDBHost()
	dbPort     = config.GetDBPort()
	dbName     = config.GetDBName()
	dbUser     = config.GetDBUser()
	dbPassword = config.GetDBPassword()
)

var db *sql.DB

func getDB() (*sql.DB, error) {

	var pqURL string

	//Check the Env
	if config.GetEnv() == "DEMO" {
		pqURL = "postgres://" + dbUser + ":" + dbPassword + "@" + dbHost + ":" + dbPort + "/" + dbName + "?sslmode=disable"
	} else {
		pqURL = "postgres://" + dbUser + ":" + dbPassword + "@" + dbHost + ":" + dbPort + "/" + dbName
	}

	_, err := connectDB(false, pqURL)
	if err != nil {
		return nil, err
	}

	err = pingDB()
	if err != nil {
		// force reconnect
		_, err = connectDB(true, pqURL)
		if err != nil {
			return nil, err
		}

		err = pingDB()
		if err != nil {
			return nil, fmt.Errorf("could not ping db: %v", err)
		}
	}

	return db, nil
}

func connectDB(reconnect bool, pqURL string) (*sql.DB, error) {
	if db == nil || reconnect {
		var err error
		db, err = sql.Open("postgres", pqURL)
		if err != nil {
			return nil, fmt.Errorf("cannot open connection to db: %v", err)
		}
	}
	return db, nil
}

func pingDB() error {
	err := db.Ping()
	if err != nil {
		return fmt.Errorf("could not ping db: %v", err)
	}
	return nil
}
